# Node JS Parsers Example

## Description

This project is an example of how to write a UDP server using the Node JS version of 
PUIs MOA Parser.  To use this server, put your version of moaParser.js and packedReader.js 
in the ./lib directory. 

The .sh scripts were written for Ubuntu linux with netcat installed.  You can open two terminal
windows.  In one of the windows:

./runServer.sh

In the other window:

./runClient.sh

... You should see the traffic parsed in the Server window.

## Notes

This example only receives a UDP packet and parses it.  Persisting the parsed data is
left as an exercise to the reader.

