/**
/odule.exports = PacketReader;
 * A MOA Parser for use with node.js
 * Joe Discar - 21 May 2019
 */
(function(exports) {

/**
 * These are the types of variables that we support out
 * of MOA.
 */
const type = {
    BYTE: 'byte',
    SIGNEDBYTE: 'signedbyte',
    SHORT: 'short',
    SIGNEDSHORT: 'signedshort',
    INT: 'int',
    SIGNEDINT: 'signedint',
    LONG: 'long',
    LLSTRING: 'llstring'
}

/**
 * How many bytes for each moa variable type
 * @param type - the MOA type your need the size of
 * @returns the number of bytes for the type
 */
this.sizeof = function(type) {
  switch(type) {
  case type.BYTE: return 1;
  case type.SHORT: return 2;
  case type.INT: return 4;
  case type.LONG: return 5;
  case type.LSTRING: return 254;
  case type.LLSTRING: return 254;
  }
  return 0;
}

/**
 * How many bytes long each type of bitmap is
 * @param typeOfMap - The MOA Map type you need the size of (mapType.MAIN, etc.)
 * @returns the number of bytes long the bitmap for that type should be
 */
this.mapSize = function(typeOfMap) {
    switch(typeOfMap) {
    case mapType.MAIN: return 3;
    case mapType.OBD: return 3;
    case mapType.EXTENDED: return 4;
    case mapType.MINOR: return 1;
    }
    return 0;
}

/**
 * The various kinds of bitmaps we support
 */
const mapType = {
    MAIN: 'main',
    OBD: 'obd',
    EXTENDED: 'extended',
    MINOR: 'minor'
}

/**
 * This defines the fields that are inside the
 * Main block, and what kind of data each is.
 * 0 = mapType
 * 1 = Bit number
 * 2 = Field Type
 * 3 = Name
 * 
 */
this.main = [
  [ mapType.MAIN, 0, type.BYTE, "seq" ],
  [ mapType.MAIN, 1, type.BYTE, "eventType" ],
  [ mapType.MAIN, 2, type.INT, "eventDate" ],
  [ mapType.MAIN, 3, type.INT, "fixDate" ],
  [ mapType.MAIN, 4, type.BYTE, "fixDelay" ],
  [ mapType.MAIN, 5, type.SIGNEDINT, "lat" ],
  [ mapType.MAIN, 6, type.SIGNEDINT, "lng" ],
  [ mapType.MAIN, 7, type.BYTE, "speed" ],
  [ mapType.MAIN, 8, type.BYTE, "heading" ],
  [ mapType.MAIN, 9, type.SHORT, "main" ],
  [ mapType.MAIN, 10, type.BYTE, "battery" ],
  [ mapType.MAIN, 11, type.BYTE, "satsFix" ],
  [ mapType.MAIN, 12, type.BYTE, "hac" ],
  [ mapType.MAIN, 13, type.BYTE, "hdop" ],
  [ mapType.MAIN, 14, type.BYTE, "reserved_14_1" ],
  [ mapType.MAIN, 15, type.SIGNEDBYTE, "rssi" ],
  [ mapType.MAIN, 16, type.BYTE, "inputStatus" ],
  [ mapType.MAIN, 17, type.BYTE, "outputStatus" ],
  [ mapType.MAIN, 18, type.BYTE, "ioState" ],
  [ mapType.MAIN, 19, type.BYTE, "vehState" ],
  [ mapType.MAIN, 20, type.LLSTRING, "eventData" ],
  [ mapType.MAIN, 21, type.BYTE, "main-21-1" ],
  [ mapType.MAIN, 22, type.BYTE, "minorMap" ],
  [ mapType.MAIN, 23, type.INT, "extendedMap" ]
];


/**
 * 
 */
this.obd = [
  [ mapType.OBD, 0, type.BYTE, "seq" ],
  [ mapType.OBD, 1, type.INT, "eventDate" ],
  [ mapType.OBD, 2, type.INT, "fixDate" ],
  [ mapType.OBD, 3, type.BYTE, "fixDelay" ],
  [ mapType.OBD, 4, type.SIGNEDINT, "lat" ],
  [ mapType.OBD, 5, type.SIGNEDINT, "lng" ],
  [ mapType.OBD, 6, type.BYTE, "speed" ],
  [ mapType.OBD, 7, type.BYTE, "heading" ],
  [ mapType.OBD, 8, type.BYTE, "satsFix" ],
  [ mapType.OBD, 9, type.BYTE, "hac" ],
  [ mapType.OBD, 10, type.BYTE, "hdop" ],
  [ mapType.OBD, 11, type.SIGNEDBYTE, "rssi" ],
  [ mapType.OBD, 12, type.LLSTRING, "vin" ],
  [ mapType.OBD, 13, type.BYTE, "obdType" ],
  [ mapType.OBD, 14, type.LLSTRING, "dtcData" ],
  [ mapType.OBD, 15, type.BYTE, "milStatus" ],
  [ mapType.OBD, 16, type.INT, "vehOdometer" ],
  [ mapType.OBD, 17, type.BYTE, "engineRuntime" ],
  [ mapType.OBD, 18, type.BYTE, "fuelType" ],
  [ mapType.OBD, 19, type.INT, "pid00" ],
  [ mapType.OBD, 20, type.INT, "pid20" ],
  [ mapType.OBD, 21, type.INT, "pid40" ],
  [ mapType.OBD, 22, type.INT, "pid60" ],
  [ mapType.OBD, 23, type.INT, "main-23-1" ]
];



/**
 *
 * These are the fields in each extended block
 */
this.extended = [
  [ mapType.EXTENDED, 0, type.BYTE, "extended_0_1" ],
  [ mapType.EXTENDED, 1, type.BYTE, "extended_1_1" ],
  [ mapType.EXTENDED, 2, type.BYTE, "extended_2_1" ],
  [ mapType.EXTENDED, 3, type.BYTE, "extended_3_1" ],
  [ mapType.EXTENDED, 4, type.BYTE, "extended_4_1" ],
  [ mapType.EXTENDED, 5, type.SHORT, "extended_5_2" ],
  [ mapType.EXTENDED, 6, type.SHORT, "extended_6_2" ],
  [ mapType.EXTENDED, 7, type.SHORT, "extended_7_2" ],
  [ mapType.EXTENDED, 8, type.SHORT, "extended_8_2" ],
  [ mapType.EXTENDED, 9, type.SHORT, "extended_9_2" ],
  [ mapType.EXTENDED, 10, type.SHORT, "extended_10_2" ],
  [ mapType.EXTENDED, 11, type.SHORT, "extended_11_2" ],
  [ mapType.EXTENDED, 12, type.SHORT, "extended_12_2" ],
  [ mapType.EXTENDED, 13, type.SHORT, "extended_13_2" ],
  [ mapType.EXTENDED, 14, type.SHORT, "extended_14_2" ],
  [ mapType.EXTENDED, 15, type.SHORT, "extended_15_2" ],
  [ mapType.EXTENDED, 16, type.SHORT, "extended_16_2" ],
  [ mapType.EXTENDED, 17, type.SHORT, "extended_17_2" ],
  [ mapType.EXTENDED, 18, type.SHORT, "extended_18_2" ],
  [ mapType.EXTENDED, 19, type.SHORT, "extended_19_2" ],
  [ mapType.EXTENDED, 20, type.INT, "extended_20_4" ],
  [ mapType.EXTENDED, 21, type.INT, "extended_21_4" ],
  [ mapType.EXTENDED, 22, type.INT, "extended_22_4" ],
  [ mapType.EXTENDED, 23, type.INT, "extended_23_4" ],
  [ mapType.EXTENDED, 24, type.INT, "extended_24_4" ],
  [ mapType.EXTENDED, 25, type.INT, "extended_25_4" ],
  [ mapType.EXTENDED, 26, type.INT, "extended_26_4" ],
  [ mapType.EXTENDED, 27, type.LLSTRING, "extended_27_254" ],
  [ mapType.EXTENDED, 28, type.LLSTRING, "extended_28_254" ],
  [ mapType.EXTENDED, 29, type.LLSTRING, "extended_29_254" ],
  [ mapType.EXTENDED, 30, type.LLSTRING, "extended_30_254" ],
  [ mapType.EXTENDED, 31, type.INT, "extended_31_4" ]
];

/**
 * Fields in the minor block
 */
this.minor = [
  [ mapType.MINOR, 0, type.INT, "minor_fixDate" ],
  [ mapType.MINOR, 1, type.BYTE, "minor_dateOffset" ],
  [ mapType.MINOR, 2, type.SIGNEDSHORT, "minor_latDiff" ],
  [ mapType.MINOR, 3, type.SIGNEDSHORT, "minor_lngDiff" ],
  [ mapType.MINOR, 4, type.SIGNEDINT, "minor_lat" ],
  [ mapType.MINOR, 5, type.SIGNEDINT, "minor_lng" ],
  [ mapType.MINOR, 6, type.BYTE, "minor_speed" ],
  [ mapType.MINOR, 7, type.BYTE, "minor_satsFix" ]
];
/*****************************************************************************************
 * The main entry point for parsing.   Simply pass it a byte array of the bytes representing
 * the message, and it will return a JSON of the parsed data.
 */
this.parse = function(bytes, config) {
  // Config will contain elements for parsing.
  if (!config) {

    
    // rationalizeData - true means that known elements (like lat) will be converted to generally accepted form like floating point
    config = { rationalizeData: false };
    config["debug"] = false;
    if (config["debug"]) {
      console.log("-- Default config");
    }
  }
  
  // Feed the bytes to the packetreader
  pr.bytes = bytes;

  // First extract the fixed header
  var checksum = pr.readShort(0);
  var imei = pr.readLong(2, 8);
  var msgType = pr.readByte(10);
  var bitmap = pr.readLong(11, 3);
  
  // Check the checksum
  var calcFletch = pr.calcFletch(2, pr.length()-2);

  // console.log("Checksum = "+checksum);
  // console.log("Imei = "+imei);
  // console.log("MsgType = "+msgType);
  // console.log("Bitmap = "+bitmap);
  
  // first parse Main
  var result = {};
  
  var pos = 14;
  result["checksum"] = checksum;

  if (checksum != calcFletch) {
    result["rcvdDate"] = Date.now();
    result["error"] = "Fletcher mismatch received "+checksum+" ("+checksum.toString(16)+") expected "+calcFletch+" ("+calcFletch.toString(16)+")";
    // return result;
    // We assume that there is no fletch on the data, and we start over again from zero
    imei = pr.readLong(0, 8);
    msgType = pr.readByte(8);
    bitmap = pr.readLong(9, 3);
    pos = 12;
  }
  

  result["imei"] = imei;
  result["msgType"] = msgType;
  result["bitmap"] = bitmap;
  
  

  
  // If the type is OBD, use the OBD map
  if ((msgType & 0x003f) == 45) {
    console.log("PARSING OBD MESSAGE");
    pos = this.decode(result, pr, mapType.OBD, bitmap, pos); // Parse the bitmap for main.
  } else {
    pos = this.decode(result, pr, mapType.MAIN, bitmap, pos); // Parse the bitmap for main.
  }
  
  // Get the extended bitmap
  
  var extendedBitmap = result.extendedMap;
  if (extendedBitmap && extendedBitmap != 0) {
    result["extended"] = [];
    var extended = {};
    while (extendedBitmap && extendedBitmap != 0) {
      // Parse the extended f
      var ext = {};
      // console.log("Extended bitmap = "+extendedBitmap+" at pos "+pos);
      pos = this.decode(ext, pr, mapType.EXTENDED, extendedBitmap, pos);
      // console.log(JSON.stringify(ext));
      Object.assign(extended, ext);
      extendedBitmap = ext.extended_31_4;
    }
    result["extended"].push(extended);
  }

  // Parse the minors if needed
  
  var minorBitmap = result.minorMap;
  if (minorBitmap && minorBitmap != 0) {
    var minors = [];
    while (minorBitmap && minorBitmap != 0 && pos < pr.length() ) {
      // Parse the extended f
      var minor = {};
      pos = this.decode(minor, pr, mapType.MINOR, minorBitmap, pos);
      // Object.assign(minors, minor);
      minors.push(minor);
      if (pos > pr.length()-6) break;
    }
    result["minors"] = minors;
  }
  
  // Common fields added to JSON:
  result["ackData"] = [ 0x2a, result["seq"]]
   
  // Rationalize data only if requested
  if (config["rationalizeData"]) {
    console.log("Rationalizing data");
    result["nonmoaType"] = result["msgType"] & 0x3f;
    result["lat"] = result["lat"] / 10000000; // lat as a float
    result["lng"] = result["lng"] / 10000000; // lng as a float
    
    // Speed is encoded as kmh up to 160.  Then add 1 for every kmh above 160
    if (result["speed"]) result["speed"] = Math.min(result["speed"], 160) + Math.max(0, (result["speed"] - 160) * 5);
    
    // Heading is in 1/255 of a degree
    if (result["heading"]) result["heading"] = result["heading"] * 360.0 / 255.0;
    
    // Main battery is encoded * 100
    if (result["main"]) result["main"] = result["main"]/100.0;
    
    // Backup battery is encoded * 10
    if (result["battery"]) result["battery"] = result["battery"] / 10.0;
    
    // Hac is encoded * 10
    if (result["hac"]) result["hac"] = result["hac"] / 10.0;
    
    // Minors are (usually) deltas from the last minor
    // The first minor is a delta from the main location
    if (result["minors"]) {
      var newMinors = [];
      var lastLat = result["lat"];
      var lastLng = result["lng"];
      var lastDate = result["eventDate"];
      var minors = result["minors"];
      minors.forEach( function(minor) {
        if (minor.minor_lat) { 
          minor["minor_lat"] = minor["minor_lat"] / 10000000.0;
        }
        if (minor.minor_lng) {
          minor["minor_lng"] = minor["minor_lng"] / 10000000.0;
        }
        if (!minor.minor_lat && !minor.minor_lng) {
          if (typeof minor["minor_latDiff"] != 'undefined') {
            minor["ratLat"] = lastLat + minor["minor_latDiff"]/1000000;
            lastLat = minor["ratLat"];
          }
          if (typeof minor["minor_lngDiff"] != 'undefined') {
            minor["ratLng"] = lastLng + minor["minor_lngDiff"]/1000000;
            lastLng = minor["ratLng"];
          }

        }
        if (typeof minor["minor_dateOffset"] != 'undefined') {
          minor["ratDate"] = lastDate + minor["minor_dateOffset"];
          lastDate = minor["ratDate"];
        }
        newMinors.push(minor)
      });
      result["minors"] = newMinors;
      
    }
    
  }
  return result;
} 

/***
 * Given a bit number, calculate which bit it is as a mask.
 * mask(3) = 0100b
 * 
 * @param bit - the bit number you need a mask for
 * @returns - the mask 2 -> 0010b
 */
this.mask = function(bit) {
  return 1 << bit;
}

/**
 * Find the field definition for a particular bit in the given type of map
 * @param typeOfMap - the mapType for the bit you want
 * @param bit - the bit number
 * @returns the field definition for that bit assuming the typeOfMap
 */
this.getFieldDefinition = function(typeOfMap, bit) {
  var map = this.main;
  switch (typeOfMap) {
  case mapType.MAIN: { map = this.main } break;
  case mapType.OBD: { map = this.obd } break;
  case mapType.EXTENDED: { map = this.extended } break;
  case mapType.MINOR: { map = this.minor } break;
  }
  
  return map[bit];  // TODO: If map is not exactly the number of bits, search iteratively!
}

/**
 *  Parse a value out of the packet reader starting at pos (position) with a field
 *  type and fieldname
 *  @param store - JSON object to store the parsed value in
 *  @param pr - Packetreader
 *  @param pos - position of the value for this field
 *  @param fieldType - the MOA type of the field
 *  @param fieldName - what the parsed value should be called.
 */
this.getParseValue = function(store, pr, pos, fieldType, fieldName) {

  var ret;
  switch (fieldType) {
  case type.BYTE: {
    ret = pr.readByte(pos);
    pos++;
  } break;
  
  case type.SIGNEDBYTE: {
    ret = pr.readSignedByte(pos);
    pos++;
  } break;
  
  case type.SHORT: { 
    // console.log("----READING SHORT FROM "+pos);
    ret = pr.readShort(pos);
    pos += 2;
  } break;
  
  case type.SIGNEDSHORT: { 
    // console.log("----READING SIGNEDSHORT FROM "+pos);
    ret = pr.readSignedShort(pos);
    pos += 2;
  } break;
  
  case type.INT: {
    ret = pr.readInt(pos);
    pos += 4;
  } break;
  
  
  case type.SIGNEDINT: {
    ret = pr.readSignedInt(pos);
    pos += 4;
  } break;
  
  
  case type.LONG: {
    ret = pr.readLong(pos,8 );
    pos += 8;
  } break;
  
  case type.LSTRING: {
    // LSTRING length is previous byte
    var s = "";
    var length = pr.readByte(pos-1);
    for (var i=0; i<length; ++i) {
      s = s + String.fromCharCode(pr.readByte(pos));
      pos++;
    }
    ret = s;
  } break;
  
  case type.LLSTRING: {
    // LLSTRING first byte is length
    var s = "";
    var length = pr.readByte(pos);
    pos ++;
    for (var i=0; i<length; ++i) {
      s = s + String.fromCharCode(pr.readByte(pos));
      pos++;
    }
    ret = s;
  } break;
  }
  
  // var record = {};
  // record[fieldName] = ret;
  // store.push(record);
  // console.log("...... storing "+ret+" in "+fieldName);
  store[fieldName] = ret;
  return pos;
}

/**
 * The decode function reads bytes from the packetreader starting at pos, and pulls
 * the values (as defined by the bitmap) that are active (assuming the bitmap represents
 * the fields as defined by the typeOfMap).  Values that are decoded are stuck into
 * the store object with the field's name.
 * 
 * @param store - JSON object to store decoded values in
 * @param pr - the packet reader of the encoded data
 * @param typeOfMap - type map type for the bitmap
 * @param bitmap - the bitmap of fields that are encoded
 * @param pos - the position to start decoding from
 * @returns - the next position to be parsed
 */
this.decode = function(store, pr, typeOfMap, bitmap, pos) {

  var sizeOfBitmap = this.mapSize(typeOfMap);
  console.log("  "+typeOfMap+" bitmap = "+bitmap+" size of map = "+sizeOfBitmap);
  for (var bit=0; bit<this.mapSize(typeOfMap)*8; ++bit) {
    if (pos >= pr.length()) break;
    if (bitmap & this.mask(bit)) {

      // This is a value
      var field = this.getFieldDefinition(typeOfMap, bit);
      console.log("Parsing "+typeOfMap+" #"+bit+" ("+field[3]+")  at pos "+pos); 
      pos = this.getParseValue(store, pr, pos, field[2], field[3]);
    }
  }
  return pos;
}

/**
 * convert a hex string into a byte array
 * @param hexString - a string of hex characters (even number, e.g. 5 should be represented by 05)
 * @returns - byte array "0a0b0c0d" -> [ 0x0a, 0x0b, 0x0c, 0x0d ]
 */
function toBytes(hexString) {
    // console.log("parsing: "+hexString);
    var result = [];
    while (hexString.length >= 2) { 
        var hex = hexString.substring(0, 2);
        result.push(parseInt(hex, 16));
        hexString = hexString.substring(2, hexString.length);
    }

    return result;

}

this.toHexString = function(byteArray) {
  return Array.from(byteArray, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('')
}


// Exports
exports.toBytes = toBytes;
exports.parse = this.parse;
exports.decode = this.decode;
exports.mapSize = this.mapSize;
exports.mask = this.mask;
exports.getFieldDefinition = this.getFieldDefinition;
exports.main = this.main;
exports.extended = this.extended;
exports.minor = this.minor;
exports.obd = this.obd;
exports.getParseValue = this.getParseValue;
exports.hello = "Hello!!!";

}) (typeof exports === 'undefined'? this['MoaParser'] = {} : exports);

