#!/bin/bash
#
# Sends a udp message to the server.  The message to send is defined
# in sample.msg
#
xxd -r -p sample.msg | nc -u localhost 10000
