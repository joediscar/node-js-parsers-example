/**
 * This module just conveniently allows you to parse through a byte array to extract
 * values.
 * Joe Discar - 21 May 2019
 */

(function(exports) {

  this.bytes = [];
  this.next = 0; // For convenience we track the next offset after we parse from this one
  
  this.length = function() {
    return this.bytes.length;
  }
  
  this.isEof = function() {
    console.log("next = "+this.next+" length = "+this.length());
    if (this.next >= this.length()) return true;
    return false;
  }
  
  this.nextPos = function() {
    return this.next;
  }
  
  this.readByte = function(start) {
    this.next = start + 1;
    val = this.bytes[start] & 0xff;
    return val;
  }
  
  this.readSignedByte = function(start) {
    this.next = start + 1;
    val = this.readSignedNum(start, 1);
    return val;
  }
  
  this.readShort = function(start) {
    var num = (this.bytes[start] << 8) | this.bytes[start+1];
    this.next = start + 2;
    return num;
  }
  
  
  this.readSignedShort = function(start) {
    return this.readSignedNum(start, 2);
  }
  
  this.readInt = function(start) {
    return this.readNum(start, 4);
  }
  
  this.readSignedInt = function(start) {
    return this.readSignedNum(start, 4);
  }
  
  this.readLong = function(start, length) {
    return this.readNum(start, length);
  }
  
  this.readNum = function(start, length) {
    var num = 0;
    for (var i=0; i<length; ++i) {
      num = (num * 256) + this.readByte(start+i);
    }
    this.next = start + length;
    return num;
  }
  
  this.readSignedNum = function(start, length) {
    var posmax = 0x0;
    for (var i=0; i<length*8-1; ++i) {
      posmax = (posmax << 1) | 0x1;
    }

    var num = this.readNum(start, length);
    if (num > posmax) {
      num = num - posmax - posmax; // Done this way to prevent overflow
    }
    return num;
  }
  
  this.readString = function(start, length) {
    var string = "";
    for (var i=0; i<length; ++i) {
      string = string + String.fromCharCode(this.bytes[start+i]);
    }
    return string;
  }
  
  this.calcFletch = function(offset, length) {
    var CK_A = 0;
    var CK_B = 0;
    
    for (var i=0; i<length; ++i) {
      CK_A = (CK_A + this.readNum(offset+i,1)) & 0xff;
      CK_B = (CK_B + CK_A) & 0xff;
    }
    return (CK_A << 8) | CK_B;
  }
  
  
  exports.readShort = this.readShort;
  exports.readLong = this.readLong;
  exports.readNum = this.readNum;
  exports.readByte = this.readByte;
  exports.calcFletch = this.calcFletch;
  exports.length = this.length;
  exports.readInt = this.readInt;
  exports.readSignedInt = this.readSignedInt;
  exports.readSignedNum = this.readSignedNum;
  exports.readSignedByte = this.readSignedByte;
  exports.readSignedShort = this.readSignedShort;
  exports.bytes = this.bytes;
  exports.hello = "Hello!!!";
  exports.isEof = this.isEof;
  
  
  
}) (typeof exports === 'undefined'?
	this['PacketReader']={} : exports);

