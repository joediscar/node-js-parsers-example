var parser = require('./lib/moaParser.js');
pr = require('./lib/packetReader.js');

var PORT = 10000;
var HOST = '0.0.0.0';
var config = { };

var dgram = require('dgram');
var server = dgram.createSocket('udp4');

// This event gets fired when the server is successfully bound to the PORT
server.on('listening', function() {
  var address = server.address();
 console.log('UDP Server listening on ' + address.address + ':' + address.port);
});

// This event gets fired when a UDP message is received.  
// The message is a byte array of the actual bytes received.
// remote is an object with information about the remote client
server.on('message', function(message, remote) {
 console.log(remote.address + ':' + remote.port +' - ' + message);
 
 // Output the message
 var s = "";
 for (var i=0; i<message.length; ++i) {
   var code = message[i];
   s = s + code.toString(16)+" ";
 }
 console.log(s);
 
 // Parse the message
 var result = parser.parse(message, config);
 
 /**
  * Print the parsed values
  */
 console.log("ANSWER:\n "+JSON.stringify(result, null, 2));
});

// Attempt to bind to the port
server.bind(PORT, HOST);